    # Ask user for a message
    print("Type een bericht:")
    message = input()

    # Print a whiteline for neatness
    print()

    # Ask user for key
    print("Kies een sleutel:")
    key = input()

    # Create a rangee with the length of the message
    ran = range(len(message))

    # Iterate through the range and therefor show all the letters
    for i in ran:
        # Get the current letters for this iteration
        currentLetter = message[i]
        currentKeyLetter = key[i % len(key)]
    
        # Get corresponding numbers
        numberLetter = ord(currentLetter)
        numberKeyLetter = ord(currentKeyLetter)

        # Add two letters
        sumOfTwoLetters = numberLetter + numberKeyLetter
    
        # Get the number of the encrypted letter
        newNumberLetter = (128+ sumOfTwoLetters - numberKeyLetter)%128
        # Get the encrypted letter based on number
        newLetter = chr(newNumberLetter)

        #print out the result
        printText = currentLetter + "(" + str(numberLetter) + ") + "
        printText += currentKeyLetter + "(" + str(numberKeyLetter) +") = "
        printText += newLetter + "(" + str(newNumberLetter) + ")"
        print(printText)
