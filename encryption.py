def Encrypt():
    print("Type het bericht dat u wilt encrypten:")
    message = input()
    print()  
    print("Kies een sleutel:")
    key = input()
    for i in range(len(message)): 
        currentLetter = message[i]
        currentKeyLetter = key[i % len(key)]       
        numberLetter = ord(currentLetter)
        numberKeyLetter = ord(currentKeyLetter)      
        sumOfTwoLetters = numberLetter + numberKeyLetter       
        newNumberLetter = sumOfTwoLetters % 128        
        newLetter = chr(newNumberLetter)
      
        print(newLetter, end="")
        
def Decrypt():  
    print("Type het bericht dat u wilt decrypten:")
    message = input()
    print()   
    print("Vul de sleutel in:")
    key = input()       
    for i in range(len(message)):
        currentLetter = message[i]
        currentKeyLetter = key[i % len(key)]          
        numberLetter = ord(currentLetter)
        numberKeyLetter = ord(currentKeyLetter)      
        newNumberLetter = (128 + numberLetter - numberKeyLetter)%128       
        newLetter = chr(newNumberLetter)
        
        print(newLetter, end="")
        
def showMenu():
    choice = input("\n\nMaak uw keuze: ")

    if choice == "1":
        Encrypt()
        showMenu()

    elif choice == "2":
        Decrypt()
        showMenu()

    elif choice not in ["1","2","3"]:
        print(choice,"Uw keuze is niet 1 van de keuzes!")
        showMenu()

print("Deze appplicatie heeft 3 keuzes.")
print("\n1. Encrypt een bericht.")
print("\n2. Decrypt een bericht.")
print("\n3. Sluit het programma.")

showMenu()
